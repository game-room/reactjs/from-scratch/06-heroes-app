import React from 'react';
import { mount } from 'enzyme'
import { HeroScreen } from '../../../components/heroes/HeroScreen';
import { Router } from 'react-router-dom';
import ReactRouter from 'react-router'

describe('Pruebas en <HeroScreen />', () => {
  const historyMock = {
    push: jest.fn(),
    replace: jest.fn(),
    location: {},
    listen: jest.fn(),
    createHref: jest.fn()
  };

  test('debe de mostrar el componente redirect si no hay argumentos en el URL', () => {
    const wrapper = mount(
      <Router history={ historyMock }>
        <HeroScreen />
      </Router>
    );

    expect( wrapper.find('Redirect').exists() ).toBe(true);
  });

  test('debe de mostrar un hero si el parámetro existe y se encuentra', () => {
    jest.spyOn(ReactRouter, 'useParams').mockReturnValue({ heroId: 'marvel-spider' });

    const wrapper = mount(
      <Router history={ historyMock }>
        <HeroScreen />
      </Router>
    );

    expect( wrapper ).toMatchSnapshot();
    expect( wrapper.find('.row').exists() ).toBe(true);
  });

  test('debe de regresar a la pantalla anterior con PUSH', () => {
    jest.spyOn(ReactRouter, 'useParams').mockReturnValue({ heroId: 'marvel-spider' });

    const historyMock = {
      push: jest.fn(),
      replace: jest.fn(),
      location: {},
      listen: jest.fn(),
      createHref: jest.fn(),
      length: 1,
      goBack: jest.fn()
    };

    const wrapper = mount(
      <Router history={ historyMock }>
        <HeroScreen />
      </Router>
    );

    wrapper.find('button').prop('onClick')();

    expect( historyMock.push ).toHaveBeenCalledWith('/');
    expect( historyMock.goBack ).not.toHaveBeenCalled();
  });

  test('debe de regresar a la pantalla anterior GOBACK', () => {
    jest.spyOn(ReactRouter, 'useParams').mockReturnValue({ heroId: 'marvel-spider' });

    const historyMock = {
      push: jest.fn(),
      replace: jest.fn(),
      location: {},
      listen: jest.fn(),
      createHref: jest.fn(),
      length: 10,
      goBack: jest.fn()
    };

    const wrapper = mount(
      <Router history={ historyMock }>
        <HeroScreen />
      </Router>
    );

    wrapper.find('button').prop('onClick')();

    expect( historyMock.goBack ).toHaveBeenCalled();
  });

  test('debe de llamar el redirect si el hero no existe', () => {
    jest.spyOn(ReactRouter, 'useParams').mockReturnValue({ heroId: 'not-matched-hero-id' });

    const historyMock = {
      push: jest.fn(),
      replace: jest.fn(),
      location: {},
      listen: jest.fn(),
      createHref: jest.fn(),
      length: 10,
      goBack: jest.fn()
    };

    const wrapper = mount(
      <Router history={ historyMock }>
        <HeroScreen />
      </Router>
    );

    expect( wrapper.text() ).toBe('');
  });
});
