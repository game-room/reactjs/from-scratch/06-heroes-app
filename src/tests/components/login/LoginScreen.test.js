import React from 'react';
import { mount } from 'enzyme';
import { LoginScreen } from '../../../components/login/LoginScreen';
import { AuthContext } from '../../../auth/AuthContext';
import { types } from '../../../types/types';
import { Router } from 'react-router-dom';

describe('Pruebas en <LoginScreen />', () => {
  const historyMock = {
    push: jest.fn(),
    replace: jest.fn(),
    location: {},
    listen: jest.fn(),
    createHref: jest.fn()
  };

  const contextValue = {
    dispatch: jest.fn(),
    user: {
      logged: false
    }
  }

  const wrapper = mount(
    <AuthContext.Provider value={ contextValue }>
      <Router history={ historyMock }>
        <LoginScreen />
      </Router>
    </AuthContext.Provider>
  )

  test('debe de mostrarse correctamente', () => {
    expect( wrapper ).toMatchSnapshot();
  });


  test('debe de realizar el dispatch y la navegación', () => {
    const handleClick = wrapper.find('button').prop('onClick');

    handleClick();

    expect( contextValue.dispatch ).toHaveBeenCalledWith({
      type: types.login,
      payload: {
        id: 123,
        name: 'Jaime'
      }
    });

    expect( historyMock.push ).toHaveBeenCalledWith('/');

    localStorage.setItem('lastPath','/dc');
    handleClick();
    expect( historyMock.push ).toHaveBeenCalledWith('/dc');
  })
})
