import React from 'react';
import { mount } from 'enzyme';
import { PrivateRoutes } from '../../routers/PrivateRoutes';
import { MemoryRouter } from 'react-router-dom';

describe('Pruebas en <PrivateRoutes />', () => {
  const props = {
    location: {
      pathname: '/marvel'
    }
  }

  Storage.prototype.setItem = jest.fn();

  test('debe de mostrar el componente si está autenticado y guardar localStorage', () => {
    const wrapper = mount(
      <MemoryRouter>
        <PrivateRoutes
          isAuthenticated={ true }
          component={ () => <span>Listo!</span> }
          { ...props }
        />
      </MemoryRouter>
    );

    expect( wrapper.find('span').exists() ).toBe(true);
    expect( localStorage.setItem ).toHaveBeenCalledWith('lastPath', '/marvel');
  });


  test('debe de bloquear el componente si no está autenticado', () => {
    const wrapper = mount(
      <MemoryRouter>
        <PrivateRoutes
          isAuthenticated={ false }
          component={ () => <span>Listo!</span> }
          { ...props }
        />
      </MemoryRouter>
    );

    expect( wrapper.find('span').exists() ).toBe(false);
    expect( localStorage.setItem ).toHaveBeenCalledWith('lastPath', '/marvel');
  });
});
