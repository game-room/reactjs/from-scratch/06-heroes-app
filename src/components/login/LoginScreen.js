import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../../auth/AuthContext';
import { types } from '../../types/types';


export const LoginScreen = () => {
  const history = useHistory();
  const { dispatch } = useContext( AuthContext );

  const handleLogin = () => {
    dispatch({
      type: types.login,
      payload: {id: 123, name: 'Jaime'}
    });

    const lastPath = localStorage.getItem('lastPath') || '/';
    //history.replace('/');
    history.push(lastPath);
  }

  return (
    <div className="container mt-5">
      <h1>Login</h1>
      <hr />

      <button
        className="btn btn-primary"
        onClick={ handleLogin }
      >
        Login
      </button>

    </div>
  )
}
