import React, { useMemo } from 'react'
import { useForm } from '../../hooks/useForm';
import { getHeroesByName } from '../../selectors/getHeroesByName';
import { HeroCard } from '../heroes/HeroCard';
import { useHistory, useLocation } from 'react-router-dom';
import queryString from 'query-string';

export const SearchScreen = () => {
  const history = useHistory();
  const location = useLocation();
  const { query = '' } = queryString.parse( location.search );

  const [ formValues, handleInputChange ] = useForm({
    searchText: query
  });

  const { searchText } = formValues;

  const heroesFiltered = useMemo(() => getHeroesByName( query ), [ query ] );

  const handleSearch = (e) => {
    e.preventDefault();
    history.push(`?query=${searchText}`)
  }


  return (
    <div className="container">
      <h1>Search Screen</h1>
      <hr />
      <div className="row">
        <div className="col">
          <h4> Search Form </h4>
          <hr />
          <form onSubmit={ handleSearch }>
            <input
              type="text"
              placeholder="Find your hero"
              className="form-control"
              name="searchText"
              autoComplete="off"
              value={ searchText }
              onChange={ handleInputChange }
            />
            <button
              type="submit"
              className="btn m-1 btn-block btn-outline-primary"
            >
              Search...
            </button>
          </form>
        </div>

        <div className="col">
          <h4> Results </h4>
          <hr />

          {
            (query ==='')
            &&
              <div className="alert alert-info">
                Search a hero
              </div>
          }

          {
            (query !=='' && heroesFiltered.length === 0 )
              &&
              <div className="alert alert-danger">
                There is no a hero with { query }
              </div>
          }

          {
            heroesFiltered.map( hero => (
              <HeroCard
                key={ hero.id }
                { ...hero }
              />
            ))
          }
        </div>
      </div>
    </div>
  )
}
